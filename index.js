/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printUser(){
		let fullName = prompt("What is your name?");
		console.log("Hello, "+fullName+"!");
		let age = prompt("How old are you?");
		console.log("You are "+age+" years old.");
		let location = prompt("Where do you live?");
		console.log("You live in "+location);
		alert("Thank you for your submission!");
	}
	printUser();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printMusicbands() {
		let artist1="1. The Script";
		console.log(artist1);
		let artist2="2. One Republic";
		console.log(artist2);
		let artist3="3. Coldplay";
		console.log(artist3);
		let artist4="4. Imagine Dragons";
		console.log(artist4);
		let artist5="5. Ben&Ben";
		console.log(artist5);
	}
	printMusicbands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayMovies(){
		let movie1="1. AVATAR: THE WAY OF WATER";
		console.log(movie1);
		console.log("Roten Tomatoes Rating: 97%")
		let movie2="2. PLANE";
		console.log(movie2);
		console.log("Roten Tomatoes Rating: 94%")
		let movie3="3. THE MENU";
		console.log(movie3);
		console.log("Roten Tomatoes Rating: 76%")
		let movie4="4. BLACK ADAM";
		console.log(movie4);
		console.log("Roten Tomatoes Rating: 88%")
		let movie5="5. BLACK PANTHER: WAKANDA FOREVER";
		console.log(movie5);
		console.log("Roten Tomatoes Rating: 94%")
	}
	displayMovies()

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// let printFriends = function printUsers(){
// 	alert("Hi! Please add the names of your friends.");
// 	let friend1 = prompt("Enter your first friend's name:"); 
// 	let friend2 = prompt("Enter your second friend's name:"); 
// 	let friend3 = prompt("Enter your third friend's name:");

// 	console.log("You are friends with:")
// 	console.log(friend1); 
// 	console.log(friend2); 
// 	console.log(friend3); 
// 	};
// 	printFriends();
